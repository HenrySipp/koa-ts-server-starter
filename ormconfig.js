module.exports = {
    "name": "default",
    "type": "sqlite",
    "database": "db.sqlite3",
    "autoSchemaSync": true,
    "synchronize": true,
    "entities": [
        "src/entity/*.ts"
    ],
    "subscribers": [
        "src/subscriber/*.ts"
    ],
    "migrations": [
        "src/migration/*.ts"
    ],
    "logging": {
        "logQueries": true
    },
    "cli": {
        "entitiesDir": "src/entity",
        "migrationsDir": "src/migration",
        "subscribersDir": "src/subscriber"
    }
}