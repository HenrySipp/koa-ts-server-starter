import discussionsRoutes from '@app/api/discussions';
import authRoutes from '@app/auth';
import { initializeAuth } from '@app/auth/initializeAuth';

import Koa, { Context } from 'koa';
import bodyParser from 'koa-bodyparser';
import passport from 'koa-passport';
import Router from 'koa-router';
import session from 'koa-session';
import 'reflect-metadata';

import { createConnection } from 'typeorm';

/**
 * Asynchronously bootstraps the application to get it running
 */
const bootstrap = async () => {

    const app = new Koa();

    // body parser
    app.use(bodyParser());

    // Sessions
    app.keys = ['secret'];
    app.use(session({}, app));

    initializeAuth();
    app.use(passport.initialize());
    app.use(passport.session());

    const router = new Router();
    const apiRouter = new Router();

    const connection = await createConnection();
    // const userRepo = connection.getRepository(User);

    app.context.database = connection;

    router.get('/', (ctx: Context, next) => {
        ctx.body = ctx.state.user;
        return next();
    });
    router.use(authRoutes);

    apiRouter.prefix('/api');
    // Require authentication for now
    apiRouter.use((ctx, next) => {
        if (ctx.isAuthenticated()) {
            return next();
        } else {
            return ctx.redirect('/');
        }
    });
    apiRouter.use(discussionsRoutes);

    app.use(router.routes());
    app.use(apiRouter.routes());

    app.listen(3001, () => console.log('API Server started'));
};

bootstrap().catch((e) => {
    console.log(e);
});
