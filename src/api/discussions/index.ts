import { Context } from 'koa';
import Router from 'koa-router';

import { getDiscussionAction } from './actions/getDiscussionAction';

const router = new Router();
router.prefix('/discuss');
router.get('/:id', getDiscussionAction);
router.get('/test', (ctx: Context) => {
    ctx.body = 'test woo';
});

export default router.routes();
