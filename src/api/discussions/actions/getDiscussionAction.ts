import { Context } from 'koa';

export function getDiscussionAction(ctx: Context) {

    const id = ctx.params.id;

    ctx.body = `Hello world. ID: ${id}`;
}
