import bcrypt from 'bcrypt';
import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm';

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: string;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column()
    email: string;

    @Column()
    password: string;

    public async validatePassword(candidate: string) {
        let res;
        try {
            res = await bcrypt.compare(candidate, this.password);
        } catch (e) {
            throw e;
        }

        return res;
    }

    public async hashPassword(password: string): Promise<string> {
        const p = new Promise<string>((res, rej) => {
            bcrypt.hash(password, 10, (err, hash) => {
                if (err) {
                    rej(err);
                }
                if (res) {
                    res(hash);
                }
            });
        });
        return p;
        // await bcrypt.hash(password, 10, (_, hash) => {
        //     this.password = hash;
        // });
    }
}
