import { User } from '@app/entity/User';
import passport from 'koa-passport';
import LocalStrategy from 'passport-local';
import { getManager } from 'typeorm';

export function initializeAuth() {

    passport.use(new LocalStrategy.Strategy({
        usernameField: 'email',
        passwordField: 'password',
    },
        async (username, password, done) => {
            try {
                const userRepo = getManager().getRepository(User);
                const user = await userRepo.findOne({
                    email: username,
                });
                if (!user) {
                    done(null, false, { message: 'Incorrect username.' });
                    return;
                }
                if (!user.validatePassword(password)) {
                    done(null, false, { message: 'Incorrect password.' });
                    return;
                }
                done(null, user);

            } catch (e) {
                console.log('asdfdsasdfad', e);
            }
        },
    ));

    passport.serializeUser((user: User, done) => { done(null, user.id); });

    passport.deserializeUser((id, done) => {
        const userRepo = getManager().getRepository(User);
        return userRepo.findOne(id)
            .then((user) => {
                if (user) {
                    const { password, ...withoutPassword } = user;
                    done(null, withoutPassword);
                } else {
                    done('user not found', undefined);
                }
            })
            .catch((err) => { done(err, undefined); });
    });

}
