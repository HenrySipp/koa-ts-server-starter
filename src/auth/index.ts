import { loginAction } from '@app/auth/actions/loginAction';
import { logoutAction } from '@app/auth/actions/logoutAction';
import { registerAction } from '@app/auth/actions/registerAction';
import Router from 'koa-router';

const router = new Router();
router.prefix('/auth');
router.post('/login', loginAction);
router.post('/register', registerAction);
router.post('/logout', logoutAction);

export default router.routes();
