import { Context } from 'koa';

export function logoutAction(ctx: Context) {
    ctx.logout();
    ctx.redirect('/');
}
