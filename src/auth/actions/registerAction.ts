import { User } from '@app/entity/User';
import { Context } from 'koa';
import { getRepository } from 'typeorm';
// import passport from 'koa-passport';

export async function registerAction(ctx: Context) {
    const params = ctx.request.body;
    const email = params.email;
    const password = params.password;
    const firstName = params.firstName;
    const lastName = params.lastName;

    if (!email || !password || !firstName || !lastName) {
        ctx.throw(400, 'Bad request parameters');
    }

    console.log(email, password, firstName, lastName);

    const user = new User();
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.password = await user.hashPassword(password);

    const userRepo = getRepository(User);
    await userRepo.save(user);

    ctx.body = 'Success';
}
